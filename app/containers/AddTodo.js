import React from "react";
import { connect } from 'react-redux'
import { 
    View, 
    TextInput, 
    Button, 
    Text, 
    StyleSheet } from 'react-native';

import { addTodo } from "../actions";


class AddTodo extends React.Component {

    render() {
        return (
            <View style={styles.container}>
                <TextInput
                    onChangeText={text => this.inputText = text}
                    width={150}
                    placeholder="Add new item" />
                <Button
                    onPress={() => {
                        console.log(this.inputText)
                        this.props.addTodoItem(this.inputText)
                        this.inputText = ''
                    }
                    }
                    style={styles.button}
                    title={"Search"} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
    },
    button: {
        marginLeft: 20,
    }
})

const mapDispatchToProps = dispatch => ({
    addTodoItem: text => {
        dispatch(addTodo(text))
    }
})

const mapStateToProps = state => ({
    
})

export default connect(mapStateToProps, mapDispatchToProps)(AddTodo)