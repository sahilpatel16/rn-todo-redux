import React from 'react'
import { View, Text } from 'react-native'
import PropTypes from 'prop-types'
import { connect } from "react-redux";

import Filter from '../components/Filter'
import { VisibilityFilters, setVisibilityFilter } from '../actions';


class FilterList extends React.Component {

    render() {

        console.log(this.props)

        return (
            <View style={{ flexDirection: 'row', }}>
                <Filter {...this.props} filterName={ VisibilityFilters.SHOW_ALL }/>
                <Filter {...this.props} filterName={ VisibilityFilters.SHOW_ACTIVE } />
                <Filter {...this.props} filterName={ VisibilityFilters.SHOW_COMPLETED } />
            </View>
        )
    }
}

const mapDispatchToProps = dispatch => ({
    updateVisibilityFilter: text => {
        dispatch(setVisibilityFilter(text))
    }
})

const mapStateToProps = state => ({
    visibilityFilterValue : state.visibilityFilter
})


export default connect(
    mapStateToProps, 
    mapDispatchToProps) (FilterList)
