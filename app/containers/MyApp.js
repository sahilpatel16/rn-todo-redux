import React from 'react'
import FilterList from './FilterList';
import TodoList from './TodoListContainer'
import AddTodo from './AddTodo';
import { View, Text, StyleSheet } from "react-native";


const MyApp = () => (
    <View style={ styles.container }>
        <AddTodo />
        <FilterList />
        <TodoList />
    </View>
)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
    },
});

export default MyApp