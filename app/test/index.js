/**
 * To test that reducers are updating state correctly when
 * a new action is dispatched.
 */
import { 
    addTodo, 
    toggleTodo, 
    setVisibilityFilter, 
    VisibilityFilters 
} from '../actions'

import { createStore } from "redux";
import rootReducer from '../reducers';

const store = createStore(rootReducer)


console.log("This is the initial state")
console.log(store.getState())

const testFlow = () => {

}

const unsubscribe = store.subscribe(() =>
    console.log(store.getState())
)

// Dispatch some actions
store.dispatch(addTodo('Learn about actions'))
store.dispatch(addTodo('Learn about reducers'))
store.dispatch(addTodo('Learn about store'))
// store.dispatch(toggleTodo(0))
// store.dispatch(toggleTodo(1))
store.dispatch(setVisibilityFilter(VisibilityFilters.SHOW_COMPLETED))

console.log(addTodo('How does it look?'))

// Stop listening to state updates
unsubscribe()

export default testFlow