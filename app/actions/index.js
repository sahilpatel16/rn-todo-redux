//	Every todo item must have an id
let nextTodoId = 0

// Types of actions 
export const ADD_TODO = 'ADD_TODO'
export const TOGGLE_TODO = 'TOGGLE_TODO'
export const SET_VISIBILITY_FILTER = 'SET_VISIBILITY_FILTER'


//	Old way of defining action creators
export function addTodo(text) {
	return {
		type : 'ADD_TODO',
		id: nextTodoId++,
		text
	}
}

export const toggleTodo = id => ({
	type: 'TOGGLE_TODO',
	id
})

export const setVisibilityFilter = filter => ({
	type: 'SET_VISIBILITY_FILTER',
	filter
})


// Defining all the constants for Action types.
export const VisibilityFilters = {
	SHOW_ALL : 'SHOW_ALL',
	SHOW_COMPLETED : 'SHOW_COMPLETED',
	SHOW_ACTIVE : 'SHOW_ACTIVE'
}