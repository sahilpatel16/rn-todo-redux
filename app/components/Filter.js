import React from 'react'
import { CheckBox, Text, StyleSheet, View } from 'react-native'
import { VisibilityFilters } from '../actions';
import PropTypes from 'prop-types'


class Filter extends React.Component {

    static getFilterName(filterKey = '') {
        if (filterKey == VisibilityFilters.SHOW_ACTIVE) {
            return 'Active'
        } 
        if (filterKey == VisibilityFilters.SHOW_COMPLETED) {
            return 'Completed'
        }
        if (filterKey == VisibilityFilters.SHOW_ALL) {
            return 'All'
        } 

        return 'Invalid'
    }

    render() {

        console.log(this.props)

        return (
            <View style={{ flexDirection: 'row' }}>
                <CheckBox
                    value={ this.props.visibilityFilterValue == this.props.filterName }
                    onValueChange={ (value) => {
                        value ? 
                            this.props.updateVisibilityFilter(this.props.filterName) :
                            this.props.updateVisibilityFilter(VisibilityFilters.SHOW_ALL)
                        }
                    }
                />
                <Text style={{ marginTop: 5 }}>{ Filter.getFilterName(this.props.filterName) }</Text>
            </View>
        )
    }
}

export default Filter