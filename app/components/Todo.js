import React from 'react'
import { Text, TouchableHighlight, StyleSheet } from 'react-native'
import PropTypes from 'prop-types'

const Todo = (props) => (
    <TouchableHighlight
        style={[styles.button]}
        onPress={
            () => props.onClick()
        }>
        <Text
            style={[
                styles.the_text, { 
                    textDecorationLine: props.completed ? 
                    'line-through' : 
                    'none' 
                }
            ]}>
            { props.text }
        </Text>
    </TouchableHighlight>
)

const styles = StyleSheet.create({
    button: {
        alignItems: 'center',
        padding: 10,
        backgroundColor: '#DDDDDD',
        alignSelf: 'stretch',
        marginLeft: 5,
        marginRight: 5,
        marginTop: 2.5,
        marginBottom: 2.5,
    },

    the_text: {
        fontSize: 30,
    }
})

export default Todo

//  Adds static checks to prop fields. This helps us to
//  find any anomalies really soon.
// Todo.propTypes = {
//     onClick: PropTypes.func.isRequired,
//     completed: PropTypes.bool.isRequired,
//     text: PropTypes.string.isRequired
// }
