.import React from 'react'
import PropTypes from 'prop-types'
import Todo from './Todo'

import { View, Text } from 'react-native'


const TodoList = (props) => (
    <View>
        {
            props.todos.map(todo => 
                <Todo
                    key={todo.id}
                    {...todo}
                    onClick={() => props.toggleTodoItem(todo.id)}
                />)
        }
    </View>
)

// TodoList.propTypes = {
//     todos: PropTypes.arrayOf(PropTypes.shape({
//         id: PropTypes.number.isRequired,
//         completed: PropTypes.bool.isRequired,
//         text: PropTypes.string.isRequired
//     }).isRequired),
//     toggleTodo: PropTypes.func.isRequired
// }

{/* <Todo
                            key={todo.id}
                            {...todo}
                            onClick={() => this.props.toggleTodo(todo.id)}
                        /> */}

export default TodoList