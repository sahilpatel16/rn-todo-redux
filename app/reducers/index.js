import { combineReducers } from 'redux'

//  Our individual reducer that only handles changes to todo object in state.
import todos from './todos'

//  Reducer which only handles state changes in visibilityFilter
import visibilityFilter from './visibilityFilter'

// https://redux.js.org/basics/reducers
const rootReducer = combineReducers({
    todos, 
    visibilityFilter
})

export default rootReducer
