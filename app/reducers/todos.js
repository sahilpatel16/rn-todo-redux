import { ADD_TODO, TOGGLE_TODO } from "../actions";
import { VisibilityFilters } from '../actions'

//  State is going to contain an array of todos.
//  action will contain a type, and any other extra data
//  related to that action
const todos = (state = [], action) => {
    
    switch (action.type) {

        //  if action type is ADD_TODO, we will return the received
        //  state with a new state appended to it.
        case ADD_TODO:
            return [

                //  Weird syntax that represents the whole list.
                ...state,

                //  The new todo object being created from action.
                {
                    id: action.id,
                    text: action.text,
                    completed: false
                }
            ]

        //  If the action has type TOGGLE_TODO, we need to go through all the 
        //  todo items and check them based on their id. When we find the desired
        //  todo item, we will toggle it's completed flag and return it. If not,
        //  we will return the same object.
        case TOGGLE_TODO:
            return state.map(todo =>
                (todo.id === action.id)
                    ? { ...todo, completed: !todo.completed }
                    : todo
            )

        //  For any other action type, return the original list.
        default:
            return state;
    }
}

export default todos