import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import MyApp from "./app/containers/MyApp";
import { Provider } from "react-redux";
import { createStore } from "redux";
import rootReducer from "./app/reducers";

export default class App extends React.Component {



  render() {
    const store = createStore(rootReducer)

    return (
      <Provider store={store}>
          <MyApp />
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
